package universite.angers.master.info.rdf.graph.model;

import org.apache.jena.ontology.OntModel;
import universite.angers.master.info.rdf.read.ReaderModel;
import universite.angers.master.info.rdf.write.WriterModel;

/**
 * Stratégie qui permet de retourner un modèle concret avec inférence
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 11/03/2020
 * @author Théo MAHAUDA, Mohamed OUHIRRA
 * @version 1.0
 *
 * @param <T> le modèle inférente maniuplé
 */
public class ModelInferenceOntologieAdapter extends ModelInferenceAdapter<OntModel> {

	public ModelInferenceOntologieAdapter(String uri, ReaderModel<OntModel> reader, WriterModel<OntModel> writer) {
		super(uri, reader, writer);
	}

	@Override
	public String toString() {
		return "ModelInferenceOntologieAdapter [model=" + model + ", uri=" + uri + ", reader=" + reader + ", writer="
				+ writer + "]";
	}
}
