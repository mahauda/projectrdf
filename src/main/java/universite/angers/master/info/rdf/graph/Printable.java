package universite.angers.master.info.rdf.graph;

/**
 * Interface qui permet d'afficher un modèle
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 11/03/2020
 * @author Théo MAHAUDA, Mohamed OUHIRRA
 * @version 1.0
 */
public interface Printable {

	/**
	 * Afficher un modèle
	 */
	public void print();
}
