package universite.angers.master.info.rdf.read;

/**
 * Interface qui permet de lire un objet
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 11/03/2020
 * @author Théo MAHAUDA, Mohamed OUHIRRA
 * @version 1.0
 *
 * @param <T> l'objet
 */
public interface Readable<T> {
	
	/**
	 * Lire un objet
	 * @return
	 */
	public T read();
}
