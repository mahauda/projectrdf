package universite.angers.master.info.rdf.graph;

import org.apache.jena.rdf.model.Model;
import org.apache.log4j.Logger;
import universite.angers.master.info.rdf.graph.model.ModelAdapter;

/**
 * Classe qui permet de regrouper dans un graphe :
 * - Le modèle qui décrit les données à travers un RDF SCHEMA
 * - Le modèle qui contient les données RDF
 * - Le modèle qui fusionne les données avec le schéma
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 11/03/2020
 * @author Théo MAHAUDA, Mohamed OUHIRRA
 * @version 1.0
 *
 * @param <T> le type de modèle pour le schéma
 * @param <K> le type de modèle pour les données
 * @param <V> le type de modèle pour les données avec schémas
 */
public class GraphAdapter<T extends Model, K extends Model, V extends Model> implements Readable, Writable, Validable, Printable {

	private static final Logger LOG = Logger.getLogger(GraphAdapter.class);
	
	/**
	 * Modele schéma qui contient les contraintes à appliquer sur le modèle des données
	 */
	private ModelAdapter<T> schema;
	
	/**
	 * Modele des données qui doivent respecter le schéma
	 */
	private ModelAdapter<K> data;
	
	/**
	 * Modèle qui contient l'union des deux modèles
	 */
	private ModelAdapter<V> dataSchema;

	public GraphAdapter(ModelAdapter<T> schema, ModelAdapter<K> data, ModelAdapter<V> dataSchema) {
		this.schema = schema;
		LOG.debug("Schema : " + this.schema);
		
		this.data = data;
		LOG.debug("Data : " + this.data);
		
		this.dataSchema = dataSchema;
		LOG.debug("Data Schema : " + this.dataSchema);
	}

	@Override
	public boolean read() {
		boolean readSchema = this.schema.read();
		LOG.debug("Read schema : " + readSchema);
		
		boolean readData = this.data.read();
		LOG.debug("Read data : " + readData);
		
		boolean readDataSchema = this.dataSchema.read();
		LOG.debug("Read data schema : " + readDataSchema);
		
		return readSchema && readData && readDataSchema;
	}
	
	@Override
	public boolean write() {
		boolean writeSchema = this.schema.write();
		LOG.debug("Write schema : " + writeSchema);
		
		boolean writeData = this.data.write();
		LOG.debug("Write data : " + writeData);
		
		boolean writeDataSchema = this.dataSchema.write();
		LOG.debug("Read data schema : " + writeDataSchema);
		
		return writeSchema && writeData && writeDataSchema;
	}
	
	@Override
	public boolean valid() {
		boolean validSchema = this.schema.valid();
		LOG.debug("Valid schema : " + validSchema);
		
		boolean validData = this.data.valid();
		LOG.debug("Valid data : " + validData);
		
		boolean validDataSchema = this.dataSchema.valid();
		LOG.debug("Valid data schema : " + validDataSchema);
		
		return validSchema && validData && validDataSchema;
	}
	
	@Override
	public void print() {
		LOG.debug("Display schema");
		this.schema.print();
		
		LOG.debug("Display data");
		this.data.print();
		
		LOG.debug("Display data schema");
		this.dataSchema.print();
	}
	
	/**
	 * @return the schema
	 */
	public ModelAdapter<T> getSchema() {
		return schema;
	}

	/**
	 * @param schema the schema to set
	 */
	public void setSchema(ModelAdapter<T> schema) {
		this.schema = schema;
	}

	/**
	 * @return the data
	 */
	public ModelAdapter<K> getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(ModelAdapter<K> data) {
		this.data = data;
	}

	/**
	 * @return the dataSchema
	 */
	public ModelAdapter<V> getDataSchema() {
		return dataSchema;
	}

	/**
	 * @param dataSchema the dataSchema to set
	 */
	public void setDataSchema(ModelAdapter<V> dataSchema) {
		this.dataSchema = dataSchema;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + ((dataSchema == null) ? 0 : dataSchema.hashCode());
		result = prime * result + ((schema == null) ? 0 : schema.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GraphAdapter<?, ?, ?> other = (GraphAdapter<?, ?, ?>) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (dataSchema == null) {
			if (other.dataSchema != null)
				return false;
		} else if (!dataSchema.equals(other.dataSchema))
			return false;
		if (schema == null) {
			if (other.schema != null)
				return false;
		} else if (!schema.equals(other.schema))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "GraphAdapter [schema=" + schema + ", data=" + data + ", dataSchema=" + dataSchema + "]";
	}
}
