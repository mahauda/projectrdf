package universite.angers.master.info.rdf.read.modelinference;

import org.apache.jena.rdf.model.InfModel;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import universite.angers.master.info.rdf.graph.model.ModelAdapter;

/**
 * Classe qui retourne un lecteur pour lire un modèle à partir de deux union de n'importe quel modèle
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 11/03/2020
 * @author Théo MAHAUDA, Mohamed OUHIRRA
 * @version 1.0
 */
public class ReaderModelInferenceFromUnionModel extends ReaderModelInference<InfModel> {

	/**
	 * Un adapter m1 ayant un modèle de n'importe quel type
	 */
	private ModelAdapter<? extends Model> m1;
	
	/**
	 * Un adapter m2 ayant un modèle de n'importe quel type
	 */
	private ModelAdapter<? extends Model> m2;
	
	public ReaderModelInferenceFromUnionModel(String path, Lang lang, 
			ModelAdapter<? extends Model> m1, ModelAdapter<? extends Model> m2) {
		super(path, lang);
		this.m1 = m1;
		this.m2 = m2;
	}

	@Override
	public InfModel read() {
		return ModelFactory.createRDFSModel(this.m1.getModel(), this.m2.getModel());
	}

	/**
	 * @return the m1
	 */
	public ModelAdapter<? extends Model> getM1() {
		return m1;
	}

	/**
	 * @param m1 the m1 to set
	 */
	public void setM1(ModelAdapter<? extends Model> m1) {
		this.m1 = m1;
	}

	/**
	 * @return the m2
	 */
	public ModelAdapter<? extends Model> getM2() {
		return m2;
	}

	/**
	 * @param m2 the m2 to set
	 */
	public void setM2(ModelAdapter<? extends Model> m2) {
		this.m2 = m2;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((m1 == null) ? 0 : m1.hashCode());
		result = prime * result + ((m2 == null) ? 0 : m2.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReaderModelInferenceFromUnionModel other = (ReaderModelInferenceFromUnionModel) obj;
		if (m1 == null) {
			if (other.m1 != null)
				return false;
		} else if (!m1.equals(other.m1))
			return false;
		if (m2 == null) {
			if (other.m2 != null)
				return false;
		} else if (!m2.equals(other.m2))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ReaderModelInferenceFromUnionModel [m1=" + m1 + ", m2=" + m2 + "]";
	}
}
