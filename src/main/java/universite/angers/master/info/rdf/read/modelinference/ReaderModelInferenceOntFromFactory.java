package universite.angers.master.info.rdf.read.modelinference;

import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;

/**
 * Classe qui retourne un lecteur pour lire un ont modèle inférent à partir de la Factory de Jena
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 11/03/2020
 * @author Théo MAHAUDA, Mohamed OUHIRRA
 * @version 1.0
 */
public class ReaderModelInferenceOntFromFactory extends ReaderModelInference<OntModel> {

	public ReaderModelInferenceOntFromFactory(String path, Lang lang) {
		super(path, lang);
	}

	@Override
	public OntModel read() {
		OntModel model = ModelFactory.createOntologyModel(OntModelSpec.RDFS_MEM_TRANS_INF);
		
		model.read("file://"+this.path, this.lang.getName());
		
		return model;
	}

	@Override
	public String toString() {
		return "ReaderModelInferenceOntFromFactory [path=" + path + ", lang=" + lang + "]";
	}	
}
