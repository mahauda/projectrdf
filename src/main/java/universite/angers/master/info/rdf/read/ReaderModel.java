package universite.angers.master.info.rdf.read;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.Lang;
import org.apache.log4j.Logger;

/**
 * Classe qui permet de lire un modèle écrit dans un langage RDF à partir d'un chemin
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 11/03/2020
 * @author Théo MAHAUDA, Mohamed OUHIRRA
 * @version 1.0
 *
 * @param <T> le type de modèle
 */
public abstract class ReaderModel<T extends Model> implements Readable<T> {

	private static final Logger LOG = Logger.getLogger(ReaderModel.class);
	
	/**
	 * Le chemin où se trouve le modèle
	 */
	protected String path;
	
	/**
	 * Le langage utilisé pour décrire le modèle
	 */
	protected Lang lang;
	
	public ReaderModel(String path, Lang lang) {
		this.path = path;
		LOG.debug("Path : " + path);
		
		this.lang = lang;
		LOG.debug("Lang : " + lang);
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * @return the lang
	 */
	public Lang getLang() {
		return lang;
	}

	/**
	 * @param lang the lang to set
	 */
	public void setLang(Lang lang) {
		this.lang = lang;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((lang == null) ? 0 : lang.hashCode());
		result = prime * result + ((path == null) ? 0 : path.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReaderModel<?> other = (ReaderModel<?>) obj;
		if (lang == null) {
			if (other.lang != null)
				return false;
		} else if (!lang.equals(other.lang))
			return false;
		if (path == null) {
			if (other.path != null)
				return false;
		} else if (!path.equals(other.path))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ReaderModel [path=" + path + ", lang=" + lang + "]";
	}
}
