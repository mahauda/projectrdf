package universite.angers.master.info.rdf.graph.model;

import java.io.OutputStream;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.riot.Lang;
import org.apache.jena.vocabulary.RDF;
import org.apache.log4j.Logger;
import universite.angers.master.info.rdf.graph.Printable;
import universite.angers.master.info.rdf.graph.Readable;
import universite.angers.master.info.rdf.graph.Validable;
import universite.angers.master.info.rdf.graph.Writable;
import universite.angers.master.info.rdf.read.ReaderModel;
import universite.angers.master.info.rdf.write.WriterModel;

/**
 * Classe qui permet d'intérargir avec un modèle et ajouter des fonctionnalités
 * supplémentaires que n'offre pas Jena
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 11/03/2020
 * @author Théo MAHAUDA, Mohamed OUHIRRA
 * @version 1.0
 *
 * @param <T> le type de modèle manipulé
 */
public abstract class ModelAdapter<T extends Model> implements Readable, Writable, Validable, Printable {

	private static final Logger LOG = Logger.getLogger(ModelAdapter.class);
	
	/**
	 * Le modèle manipulé
	 */
	protected T model;
	
	/**
	 * L'URI de base qui identifie le modèle
	 * Ex : http://www.m1wd.org/catalogue/techno
	 */
	protected String uri;
	
	/**
	 * Le lecteur qui permet de lire le modèle à partir d'un format (fichier, etc.)
	 */
	protected ReaderModel<T> reader;
	
	/**
	 * L'écriteur qui permet d'écrire le modèle dans un format (fichier, etc.)
	 */
	protected WriterModel<T> writer;
	
	public ModelAdapter(String uri, ReaderModel<T> reader, WriterModel<T> writer) {
		this.uri = uri;
		LOG.debug("URI : " + this.uri);
		
		this.reader = reader;
		LOG.debug("Reader : " + this.reader);
		
		this.writer = writer;
		LOG.debug("Writer : " + this.writer);
	}
	
	@Override
	public boolean read() {
		this.model = this.reader.read();
		LOG.debug("Read model : " + model);
		return this.model != null;
	}

	@Override
	public boolean write() {
		LOG.debug("Write model : " + this.model);
		return this.writer.write(this.model);
	}
	
	@Override
	public void print() {
		//Affiche sur la sortie standard le modèle dans la langue définit dans le lecteur
		this.print(System.out, this.reader.getLang());
	}
	
	/**
	 * Ecrit le modèle dans une sortie (console, fichier, etc.)
	 * @param os le format de sortie
	 * @param lang la langue qui sera sortie (N3, RDF-XML)
	 */
	public void print(OutputStream os, Lang lang) {
		this.model.write(os, lang.getName());
	}
	
	/**
	 * Récupérer un sujet dans le modèle avec la base de l'URI
	 * @param name
	 * @return
	 */
	public Resource getSubject(String name) {
		return this.model.getResource(this.uri + "#" + name);
	}
	
	/**
	 * Récupérer un prédicat dans le modèle avec la base de l'URI
	 * @param name
	 * @return
	 */
	public Property getPredicat(String name) {
		return this.model.getProperty(this.uri + "#" + name);
	}
	
	/**
	 * Récupérer un object ou littéral dans le modèle avec la base de l'URI
	 * @param name
	 * @return
	 */
	public Property getObject(String name) {
		return this.model.getProperty(this.uri + "#" + name);
	}
	
	/**
	 * Vérifier si un sujet est de type
	 * @param subject
	 * @param type
	 * @return
	 */
	public boolean resourceContainsType(Resource subject, Resource type) {
		
		StmtIterator iterType = subject.listProperties(RDF.type);
			
		//On parcours tout les types de la photo
		while(iterType.hasNext()) {
			
			Statement stmtType = iterType.nextStatement();
			RDFNode objectType = stmtType.getObject();
			
			//Si le sujet contient le type
			if(objectType.asResource().equals(type)) {
				return true;
			}
		}
		
		return false;
	}

	/**
	 * @return the uri
	 */
	public String getUri() {
		return uri;
	}

	/**
	 * @param uri the uri to set
	 */
	public void setUri(String uri) {
		this.uri = uri;
	}

	/**
	 * @return the model
	 */
	public T getModel() {
		return model;
	}

	/**
	 * @param model the model to set
	 */
	public void setModel(T model) {
		this.model = model;
	}

	/**
	 * @return the reader
	 */
	public ReaderModel<T> getReader() {
		return reader;
	}

	/**
	 * @param reader the reader to set
	 */
	public void setReader(ReaderModel<T> reader) {
		this.reader = reader;
	}

	/**
	 * @return the writer
	 */
	public WriterModel<T> getWriter() {
		return writer;
	}

	/**
	 * @param writer the writer to set
	 */
	public void setWriter(WriterModel<T> writer) {
		this.writer = writer;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((model == null) ? 0 : model.hashCode());
		result = prime * result + ((reader == null) ? 0 : reader.hashCode());
		result = prime * result + ((uri == null) ? 0 : uri.hashCode());
		result = prime * result + ((writer == null) ? 0 : writer.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ModelAdapter<?> other = (ModelAdapter<?>) obj;
		if (model == null) {
			if (other.model != null)
				return false;
		} else if (!model.equals(other.model))
			return false;
		if (reader == null) {
			if (other.reader != null)
				return false;
		} else if (!reader.equals(other.reader))
			return false;
		if (uri == null) {
			if (other.uri != null)
				return false;
		} else if (!uri.equals(other.uri))
			return false;
		if (writer == null) {
			if (other.writer != null)
				return false;
		} else if (!writer.equals(other.writer))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ModelAdapter [uri=" + uri + ", model=" + model + ", reader=" + reader + ", writer=" + writer + "]";
	}
}
