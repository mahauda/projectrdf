package universite.angers.master.info.rdf.write;

/**
 * Interface qui permet d'écrire un objet
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 11/03/2020
 * @author Théo MAHAUDA, Mohamed OUHIRRA
 * @version 1.0
 *
 * @param <T> le type de l'objet
 */
public interface Writable<T> {
	
	/**
	 * Ecrire un objet
	 * @param content
	 * @return
	 */
	public boolean write(T content);
}
