package universite.angers.master.info.rdf.graph;

import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.InfModel;
import org.apache.jena.riot.Lang;
import universite.angers.master.info.rdf.graph.model.ModelAdapter;
import universite.angers.master.info.rdf.graph.model.ModelInferenceAdapter;
import universite.angers.master.info.rdf.graph.model.ModelInferenceOntologieAdapter;
import universite.angers.master.info.rdf.read.ReaderModel;
import universite.angers.master.info.rdf.read.modelinference.ReaderModelInferenceFromUnionModel;
import universite.angers.master.info.rdf.read.modelinference.ReaderModelInferenceOntFromFactory;
import universite.angers.master.info.rdf.write.WriterModel;

/**
 * Factory qui permet de construire un graphe avec des modèles
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 11/03/2020
 * @author Théo MAHAUDA, Mohamed OUHIRRA
 * @version 1.0
 */
public class GraphFactory {

	private GraphFactory() {

	}

	/**
	 * Construit un OntModele inférente
	 * @param path
	 * @param lang
	 * @param uri
	 * @return
	 */
	public static ModelAdapter<OntModel> buildOntModelFromFactory(String path, Lang lang, String uri) {
		//Le lecteur
		ReaderModel<OntModel> readerOntModelSchema = new ReaderModelInferenceOntFromFactory(path, lang);
		
		//Le rédacteur
		WriterModel<OntModel> writerOntModelSchema = new WriterModel<>(path, lang);
		
		//Le modèle
		return new ModelInferenceOntologieAdapter(uri, readerOntModelSchema, writerOntModelSchema);
	}
	
	/**
	 * Construit un modèle inférente abstrait à partir de deux OntModel
	 * @param path
	 * @param lang
	 * @param uri
	 * @param schema
	 * @param data
	 * @return
	 */
	public static ModelAdapter<InfModel> buildInfModelFromUnionOntModel(String path, Lang lang, String uri,
			ModelAdapter<OntModel> schema, ModelAdapter<OntModel> data) {
		
		//Le lecteur
		ReaderModel<InfModel> readerInfModelDataSchema = new ReaderModelInferenceFromUnionModel(path, lang, schema, data);
		
		//Le rédacteur
		WriterModel<InfModel> writerInfModelDataSchema = new WriterModel<>(path, lang);
		
		//Le modèle
		return new ModelInferenceAdapter<>(uri, readerInfModelDataSchema, writerInfModelDataSchema);
	}
	
	/**
	 * Construit un graph
	 * @param pathSchema
	 * @param langSchema
	 * @param uriSchema
	 * @param pathData
	 * @param langData
	 * @param uriData
	 * @param pathDataSchema
	 * @param langDataSchema
	 * @param uriDataSchema
	 * @return
	 */
	public static GraphAdapter<OntModel, OntModel, InfModel> buildGraphOntModelFromFactory(
			String pathSchema, Lang langSchema, String uriSchema,
			String pathData, Lang langData, String uriData,
			String pathDataSchema, Lang langDataSchema, String uriDataSchema) {
		
		// On récupère le modèle schéma
		ModelAdapter<OntModel> schema = buildOntModelFromFactory(pathSchema, langSchema, uriSchema);

		// On récupère le modèle des données
		ModelAdapter<OntModel> data = buildOntModelFromFactory(pathData, langData, uriData);

		// On récupère le modèle inférentes des données issu de schéma et data
		ModelAdapter<InfModel> dataSchema = buildInfModelFromUnionOntModel(pathDataSchema, langDataSchema, uriDataSchema,
				schema, data);
		
		return new GraphAdapter<>(schema, data, dataSchema);
	}
}
