package universite.angers.master.info.rdf;

import org.apache.jena.rdf.model.*;
import org.apache.jena.util.*;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.reasoner.*;
import java.util.*;

public class TestOwlLaboratoire {
	
	private static final String PATH_SCHEMA = "file:///media/etudiant/2DCF15A2510DBD36/Cours/Master/M1/Web_Donnees/TP/TP2/workspace/ProjectRDF/src/main/resources/rdf/organisation/Organisation_Schema.owl";
	private static final String PATH_DATA = "file:///media/etudiant/2DCF15A2510DBD36/Cours/Master/M1/Web_Donnees/TP/TP2/workspace/ProjectRDF/src/main/resources/rdf/organisation/Organisation_Data.owl";
	private static InfModel infModel;
	
	private static void printStatements(Model m, Resource s, Property p, Resource o) {
		for (StmtIterator i = m.listStatements(s, p, o); i.hasNext();) {
			Statement stmt = i.nextStatement();
			System.out.println(" - " + PrintUtil.print(stmt));
		}
	}
	
	private static void load() {
		// Chargement des documents.
		Model schema = FileManager.get().loadModel(PATH_SCHEMA);
		Model data = FileManager.get().loadModel(PATH_DATA);
				
		// Initialisation du reasoner.
		Reasoner reasoner = ReasonerRegistry.getOWLReasoner();
		reasoner = reasoner.bindSchema(schema);
		infModel = ModelFactory.createInfModel(reasoner, data);
	}
	
	private static void displayAllTriplets(String subject) {
		Resource resource = infModel.getResource(subject);
		System.out.println("All triplets : ");
		printStatements(infModel, resource, null, null);
	}
	
	private static void displayAllClass(String instance) {
		Resource resource = infModel.getResource(instance);
		System.out.println("All class : ");
		printStatements(infModel, resource, RDF.type, null);
	}
	
	private static void displayAllSubject(String type) {
		Resource resource = infModel.getResource(type);
		System.out.println("All Subject : ");
		printStatements(infModel, null, RDF.type, resource);
	}
	
	private static void validate() {
		System.out.println("Valid : ");
		ValidityReport validity = infModel.validate();
		if (validity.isValid())
			System.out.println("OK");
		else {
			System.out.println("Erreurs");
			for (Iterator<ValidityReport.Report> i = validity.getReports(); i.hasNext();) {
				ValidityReport.Report report = i.next();
				System.out.println(" - " + report);
			}
		}
	}
	
	public static void main(String args[]) {
		load();
		
		/**
		 * Créer dans le fichier RDF un membre du laboratoire qui est un Administratif et qui fait les TP de BD en
		 * M1. Vérifier la validité du document à l’aide d’un programme. Expliquer l’(éventuelle) erreur.
		 * 
		 * - Error ("conflict"): "Individual a member of disjoint classes"
		 * Culprit = <http://www.m1wd.org/organisation/laboratoire#Admin>
		 * Implicated node: <http://www.m1wd.org/organisation/laboratoire#Administratif>
		 * Implicated node: <http://www.m1wd.org/organisation/laboratoire#Enseignant>
		 * 
		 *  - Error ("conflict"): "Two individuals both same and different, may be due to disjoint classes or functional properties"
		 *  Culprit = <http://www.m1wd.org/organisation/laboratoire#Admin>
		 *  Implicated node: <http://www.m1wd.org/organisation/laboratoire#Admin>
		 *  
		 *  Le programme considère que l'administratif est un enseignant. Or c'est faux puisqu'ils sont disjoints
		 *  "Un enseignant ne peut pas être un administratif"
		 */
		
		validate();
		
		/**
		 * Dans cette question et les suivantes, on suppose que l’ontologie et les connaissances sont celles des
		 * exercices 1 et 2. Afficher toutes les classes dont Marcel est instance. Comprendre ce qui est affiché.
		 * 
		 *  - (<http://www.m1wd.org/organisation/laboratoire#MarcelPigeon> rdf:type <http://www.m1wd.org/organisation/laboratoire#Membre>)
		 *  - (<http://www.m1wd.org/organisation/laboratoire#MarcelPigeon> rdf:type owl:NamedIndividual)
		 *  - (<http://www.m1wd.org/organisation/laboratoire#MarcelPigeon> rdf:type <http://www.m1wd.org/organisation/laboratoire#Personne>)
		 *  - (<http://www.m1wd.org/organisation/laboratoire#MarcelPigeon> rdf:type owl:Thing)
		 *  - (<http://www.m1wd.org/organisation/laboratoire#MarcelPigeon> rdf:type <http://www.m1wd.org/organisation/laboratoire#Auteur>)
		 *  - (<http://www.m1wd.org/organisation/laboratoire#MarcelPigeon> rdf:type <http://www.m1wd.org/organisation/laboratoire#Enseignant>)
		 *  - (<http://www.m1wd.org/organisation/laboratoire#MarcelPigeon> rdf:type rdfs:Resource)
		 *  - (<http://www.m1wd.org/organisation/laboratoire#MarcelPigeon> rdf:type <http://www.m1wd.org/organisation/laboratoire#PRAG>)
		 * 
		 * Marcel est :
		 * - Un membre du laboratoire car il fait partie d'une sous équipe
		 * - Auteur car il a publié une revue international
		 * - Enseignant car il fait le cours de BD
		 * - PRAG car c'est un enseignant
		 */
		displayAllClass("http://www.m1wd.org/organisation/laboratoire#MarcelPigeon");
		
		/**
		 * Afficher toutes les publications de haut niveau
		 * 
		 * (<http://www.m1wd.org/organisation/laboratoire#KR2012> rdf:type <http://www.m1wd.org/organisation/laboratoire#PublicationHautNiveau>)
		 * 
		 * En effet KR2012 est une revue publié à l'international : "Toute publication effectuée dans une revue internationale est une publication de haut niveau"
		 */
		displayAllSubject("http://www.m1wd.org/organisation/laboratoire#PublicationHautNiveau");
		
		/**
		 * Créer une publication de haut niveau effectuée dans
		 * une conférence nationale. Vérifier la validité du document. Une publication de haut niveau étant
		 * internationale, comprendre pourquoi une erreur est détectée (ou pas) et corriger ce qui doit l’être, si
		 * nécessaire.
		 */
	}
}
