package universite.angers.master.info.rdf;

import org.apache.jena.rdf.model.*;
import org.apache.jena.util.*;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.reasoner.*;
import java.util.*;

public class TestOwlOrdinateur {
	
	public static void printStatements(Model m, Resource s, Property p, Resource o) {
		for (StmtIterator i = m.listStatements(s, p, o); i.hasNext();) {
			Statement stmt = i.nextStatement();
			System.out.println(" - " + PrintUtil.print(stmt));
		}
	}

	public static void main(String args[]) {
		// Chargement des documents.
		Model schema = FileManager.get().loadModel("file:///media/etudiant/2DCF15A2510DBD36/Cours/Master/M1/Web_Donnees/TP/TP2/workspace/ProjectRDF/src/main/resources/rdf/ordinateurs/Ordinateurs_Schema.owl");
		Model data = FileManager.get().loadModel("file:///media/etudiant/2DCF15A2510DBD36/Cours/Master/M1/Web_Donnees/TP/TP2/workspace/ProjectRDF/src/main/resources/rdf/ordinateurs/Ordinateurs_Data.rdf");
		
		// Initialisation du reasoner.
		Reasoner reasoner = ReasonerRegistry.getOWLReasoner();
		reasoner = reasoner.bindSchema(schema);
		InfModel infmodel = ModelFactory.createInfModel(reasoner, data);

		// 1. Reconnaissance d'instances.
		System.out.println("***1***");
		Resource odj = infmodel.getResource("http://www.exemple.com#OrdinateurDeJeu");
		Resource uo = infmodel.getResource("http://www.exemple.com#unordi");
		if (infmodel.contains(uo, RDF.type, odj))
			System.out.println("unordi est une instance de OrdinateurDeJeu");
		else
			System.out.println("unordi n'est pas une instance de OrdinateurDeJeu");

		// 2. Triplets (inférés) ayant unecartemere comme sujet.
		System.out.println("***2***");
		Resource ucm = infmodel.getResource("http://www.exemple.com#unecartemere");
		System.out.println("unecartemere:");
		printStatements(infmodel, ucm, null, null);

		// 3. Egalité d'instances de France.
		System.out.println("***3***");
		Resource fra = infmodel.getResource("http://www.exemple.com#France");
		System.out.println("France:");
		printStatements(infmodel, fra, OWL.sameAs, null);

		// 4. Recherche d'inconsistances.
		System.out.println("***4***");
		ValidityReport validity = infmodel.validate();
		if (validity.isValid())
			System.out.println("OK");
		else {
			System.out.println("Erreurs");
			for (Iterator<ValidityReport.Report> i = validity.getReports(); i.hasNext();) {
				ValidityReport.Report report = i.next();
				System.out.println(" - " + report);
			}
		}
	}
}
