package universite.angers.master.info.rdf;

import org.apache.jena.ontology.OntModel;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.InfModel;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.riot.Lang;
import org.apache.jena.vocabulary.RDF;
import org.apache.log4j.Logger;
import universite.angers.master.info.rdf.graph.GraphAdapter;
import universite.angers.master.info.rdf.graph.model.ModelAdapter;
import universite.angers.master.info.rdf.graph.model.ModelInferenceAdapter;
import universite.angers.master.info.rdf.graph.model.ModelInferenceOntologieAdapter;
import universite.angers.master.info.rdf.read.ReaderModel;
import universite.angers.master.info.rdf.read.modelinference.ReaderModelInferenceFromUnionModel;
import universite.angers.master.info.rdf.read.modelinference.ReaderModelInferenceOntFromFactory;
import universite.angers.master.info.rdf.write.WriterModel;

/**
 * Test sur les données des composants informatiques à travers des exercices
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 11/03/2020
 * @author Théo MAHAUDA, Mohamed OUHIRRA
 * @version 1.0
 */
public class TestComposants {

	private static final Logger LOG = Logger.getLogger(TestComposants.class);

	/**
	 * Propriétés schéma
	 */
	private static final String URI_CM_SCHEMA = "http://www.m1wd.org/catalogue/techno";
	private static final String PATH_CM_SCHEMA_N3 = "/media/etudiant/2DCF15A2510DBD36/Cours/Master/M1/Web_Donnees/TP/TP2/workspace/ProjectRDF/src/main/resources/rdf/composants/Composants_Schema.n3";
	private static final String PATH_CM_SCHEMA_RDFXML = "/media/etudiant/2DCF15A2510DBD36/Cours/Master/M1/Web_Donnees/TP/TP2/workspace/ProjectRDF/src/main/resources/rdf/composants/Composants_Schema_Convertit.n3";

	/**
	 * Propriétés data
	 */
	private static final String URI_CM_DATA = "http://www.m1wd.org/catalogue/techno";
	private static final String PATH_CM_DATA_N3 = "/media/etudiant/2DCF15A2510DBD36/Cours/Master/M1/Web_Donnees/TP/TP2/workspace/ProjectRDF/src/main/resources/rdf/composants/Composants_Data.n3";
	private static final String PATH_CM_DATA_RDFXML = "/media/etudiant/2DCF15A2510DBD36/Cours/Master/M1/Web_Donnees/TP/TP2/workspace/ProjectRDF/src/main/resources/rdf/composants/Composants_Data_Convertit.n3";
	
	/**
	 * Propriétés data schéma
	 */
	private static final String URI_CM_DATA_SCHEMA = "http://www.m1wd.org/catalogue/techno";
	private static final String PATH_CM_DATA_SCHEMA_N3 = "/media/etudiant/2DCF15A2510DBD36/Cours/Master/M1/Web_Donnees/TP/TP2/workspace/ProjectRDF/src/main/resources/rdf/composants/Composants_Data_Schema.n3";
	private static final String PATH_CM_DATA_SCHEMA_RDFXML = "/media/etudiant/2DCF15A2510DBD36/Cours/Master/M1/Web_Donnees/TP/TP2/workspace/ProjectRDF/src/main/resources/rdf/composants/Composants_Data_Schema_Convertit.n3";

	/**
	 * Le graph qui contient les trois modèles
	 */
	private static GraphAdapter<OntModel, OntModel, InfModel> graphCM;

	public static void main(String[] args) {
		load();
		converterLangN3ToRDFXML();
		checkGraph();
		addMateriels();
		displayMaterielsCarteMereCompatibleWithCarteGraphique();
		displayAllMaterielsAvailableFromJava();
		displayAllMaterielsAvailableFromSPARQL();
	}

	/**
	 * Charge le graphe des données et des schémas
	 */
	private static void load() {
		// On récupère le modèle schéma
		ReaderModel<OntModel> readerOntModelSchema = new ReaderModelInferenceOntFromFactory(PATH_CM_SCHEMA_N3, Lang.N3);
		WriterModel<OntModel> writerOntModelSchema = new WriterModel<>(PATH_CM_SCHEMA_RDFXML, Lang.RDFXML);
		ModelAdapter<OntModel> adapterOntModelSchema = new ModelInferenceOntologieAdapter(URI_CM_SCHEMA, readerOntModelSchema, writerOntModelSchema);

		// On récupère le modèle des données
		ReaderModel<OntModel> readerOntModelData = new ReaderModelInferenceOntFromFactory(PATH_CM_DATA_N3, Lang.N3);
		WriterModel<OntModel> writerOntModelData = new WriterModel<>(PATH_CM_DATA_RDFXML, Lang.RDFXML);
		ModelAdapter<OntModel> adapterOntModelData = new ModelInferenceOntologieAdapter(URI_CM_DATA, readerOntModelData, writerOntModelData);

		// On récupère le modèle des données
		ReaderModel<InfModel> readerInfModelDataSchema = new ReaderModelInferenceFromUnionModel(PATH_CM_DATA_SCHEMA_N3, Lang.N3, adapterOntModelSchema, adapterOntModelData);
		WriterModel<InfModel> writerInfModelDataSchema = new WriterModel<>(PATH_CM_DATA_SCHEMA_RDFXML, Lang.RDFXML);
		ModelAdapter<InfModel> adapterInfModelDataSchema = new ModelInferenceAdapter<>(URI_CM_DATA_SCHEMA, readerInfModelDataSchema, writerInfModelDataSchema);
		
		graphCM = new GraphAdapter<>(adapterOntModelSchema, adapterOntModelData, adapterInfModelDataSchema);
		graphCM.read();
		graphCM.print();
	}

	/**
	 * Écrire un programme basé sur Jena qui charge un document N3 et qui affiche
	 * sur la sortie standard le graphe RDF chargé en RDF XML
	 */
	private static void converterLangN3ToRDFXML() {
		boolean read = graphCM.read();
		LOG.debug("Graph read to N3 : " + read);
		
		boolean write = graphCM.write();
		LOG.debug("Graph write to RDF XML : " + write);
	}

	/**
	 * Utiliser ce programme pour vérifier les fichiers écrits dans la feuille de TP
	 * 1. Remarquer l’usage du prédicat a qui peut être utilisé en Notation 3 à la
	 * place de rdf:type
	 */
	private static void checkGraph() {
		boolean valid = graphCM.valid();
		LOG.debug("Graph is valid : " + valid);
	}

	/**
	 * Rajouter à la base de matériels une carte mère P4P800 disposant d'un port AGP
	 * et d'un port PCI. Ajouter une carte graphique GF5200 disposant d'un port AGP
	 * et d'une sortie VGA.
	 */
	private static void addMateriels() {
		// On récupère les sujets

		Resource cm = graphCM.getDataSchema().getSubject("CM");
		LOG.debug("Subject CM : " + cm);

		Resource cg = graphCM.getDataSchema().getSubject("CG");
		LOG.debug("Subject CG : " + cm);

		// On récupère les prédicats nom, apourport, apoursortie

		Property nom = graphCM.getDataSchema().getPredicat("nom");
		LOG.debug("Predicat nom : " + nom);

		Property port = graphCM.getDataSchema().getPredicat("apourport");
		LOG.debug("Predicat port : " + port);

		Property sortie = graphCM.getDataSchema().getPredicat("apoursortie");
		LOG.debug("Predicat sortie : " + sortie);

		// On récupère les objets des ports AGP et PCI, VGA

		Property portAGP = graphCM.getDataSchema().getObject("AGP");
		LOG.debug("Property port AGP : " + portAGP);

		Property portPCI = graphCM.getDataSchema().getObject("PCI");
		LOG.debug("Property port PCI : " + portPCI);

		Property sortieVGA = graphCM.getDataSchema().getObject("VGA");
		LOG.debug("Property port VGA : " + sortieVGA);

		// Carte mère P4P800 disposant d'un port AGP et d'un port PCI.
		graphCM.getDataSchema().getModel().createResource(URI_CM_DATA + "#P4P800", cm)
				.addLiteral(nom, "P4P800")
				.addProperty(port, portAGP)
				.addProperty(port, portPCI);

		// Carte graphique GF5200 disposant d'un port AGP et d'une sortie VGA.
		graphCM.getDataSchema().getModel().createResource(URI_CM_DATA + "#GF5200", cg)
				.addLiteral(nom, "GF5200")
				.addProperty(port, portAGP)
				.addProperty(sortie, sortieVGA);

		graphCM.getDataSchema().print();
	}

	/**
	 * Écrire un programme basé sur Jena qui charge un document RDF contenant la
	 * base des matériels et qui pour tous les couples (carte mère, carte graphique)
	 * affiche si la carte graphique est compatible avec la carte mère sous la forme
	 * suivante :
	 */
	private static void displayMaterielsCarteMereCompatibleWithCarteGraphique() {

		Resource cm = graphCM.getDataSchema().getSubject("CM");
		LOG.debug("Subject CM : " + cm);

		Resource cg = graphCM.getDataSchema().getSubject("CG");
		LOG.debug("Subject CG : " + cg);

		Property port = graphCM.getDataSchema().getPredicat("apourport");
		LOG.debug("Predicat port : " + port);

		Property nom = graphCM.getDataSchema().getPredicat("nom");
		LOG.debug("Predicat nom : " + nom);

		// Itérateur carte mère
		StmtIterator iterCM = graphCM.getDataSchema().getModel().listStatements(null, RDF.type, cm);

		// On parcourt chaque carte mère
		while (iterCM.hasNext()) {

			Statement stmtCM = iterCM.nextStatement();
			Resource subjectCM = stmtCM.getSubject();
			Property predicateCM = stmtCM.getPredicate();
			RDFNode objectCM = stmtCM.getObject();

			LOG.debug("Stmt CM : " + subjectCM);
			LOG.debug("Subject CM : " + subjectCM);
			LOG.debug("Predicate CM : " + predicateCM);
			LOG.debug("Object CM : " + objectCM);

			// Itérateur carte graphique
			StmtIterator iterCG = graphCM.getDataSchema().getModel().listStatements(null, RDF.type, cg);

			// On parcours chaque carte graphique
			while (iterCG.hasNext()) {

				Statement stmtCG = iterCG.nextStatement();
				Resource subjectCG = stmtCG.getSubject();
				Property predicateCG = stmtCG.getPredicate();
				RDFNode objectCG = stmtCG.getObject();

				LOG.debug("Stmt CG : " + subjectCG);
				LOG.debug("Subject CG : " + subjectCG);
				LOG.debug("Predicate CG : " + predicateCG);
				LOG.debug("Object CG : " + objectCG);

				// On ne récupère que les propriétés "apourport" pour la CM
				StmtIterator iterPortCM = subjectCM.listProperties(port);

				// On parcours tous les ports de la carte mere courante
				while (iterPortCM.hasNext()) {

					Statement stmtPortCM = iterPortCM.nextStatement();
					Resource subjectPortCM = stmtPortCM.getSubject();
					Property predicatePortCM = stmtPortCM.getPredicate();
					RDFNode objectPortCM = stmtPortCM.getObject();

					LOG.debug("Stmt port CM : " + subjectPortCM);
					LOG.debug("Subject port CM : " + subjectPortCM);
					LOG.debug("Predicate port CM : " + predicatePortCM);
					LOG.debug("Object port CM : " + objectPortCM);

					// On ne récupère que les propriétés "apourport" pour la CG
					StmtIterator iterPortCG = subjectCG.listProperties(port);
					
					while (iterPortCG.hasNext()) {

						Statement stmtPortCG = iterPortCG.nextStatement();
						Resource subjectPortCG = stmtPortCG.getSubject();
						Property predicatePortCG = stmtPortCG.getPredicate();
						RDFNode objectPortCG = stmtPortCG.getObject();

						LOG.debug("Stmt port CG : " + subjectPortCG);
						LOG.debug("Subject port CG : " + subjectPortCG);
						LOG.debug("Predicate port CG : " + predicatePortCG);
						LOG.debug("Object port CG : " + objectPortCG);

						// Si le noeud est le meme
						if (objectPortCM.equals(objectPortCG)) {
							String nameCM = subjectCM.getProperty(nom).getString();
							LOG.debug("Name CM : " + nameCM);

							String nameCG = subjectCG.getProperty(nom).getString();
							LOG.debug("Name CG : " + nameCG);

							LOG.debug("Carte mère " + nameCM + " compatible avec carte graphique " + nameCG);
							LOG.debug("Port : " + objectPortCG);
						}
					}
				}
			}
		}
	}

	/**
	 * Écrire un programme basé sur Jena qui charge le RDF-Schema, une base
	 * d'exemple de matériel construite sur ce RDF-Schema, vérifie la validité de la
	 * base, et affiche à l'écran les noms de tous les matériels en vente (quel que
	 * soit leur type). Pour cela, faire un parcours des triplets du modèle qui
	 * définissent les matériels.
	 */
	private static void displayAllMaterielsAvailableFromJava() {
		//On récupère le type matériel = Composant
		Resource composant = graphCM.getDataSchema().getSubject("Composant");
		LOG.debug("Subject composant : " + composant);
		
		StmtIterator iterMateriels = graphCM.getDataSchema().getModel().listStatements(null, RDF.type, composant);

		// On parcourt tous les matériels
		while (iterMateriels.hasNext()) {

			Statement stmtMateriel = iterMateriels.nextStatement();
			Resource subjectMateriel = stmtMateriel.getSubject();

			LOG.debug("Materiel available : " + subjectMateriel);
		}
	}

	/**
	 * Extraire les mêmes informations que pour la question précédente en exécutant
	 * depuis votre code Java utilisant Jena une requête SPARQL (et sans faire appel
	 * à bin/sparql , évidemment).
	 */
	private static void displayAllMaterielsAvailableFromSPARQL() {
		String queryString = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" + 
				"PREFIX techno: <http://www.m1wd.org/catalogue/techno#>\n" + 
				"\n" + 
				"SELECT ?nom\n" + 
				"WHERE {\n" + 
				"?nom rdf:type techno:Composant .\n" + 
				"}";

		Query query = QueryFactory.create(queryString);
		QueryExecution qexec = QueryExecutionFactory.create(query, graphCM.getDataSchema().getModel());

		try {
			ResultSet results = qexec.execSelect();
			LOG.debug("Results : " + results);

			while (results.hasNext()) {
				QuerySolution soln = results.nextSolution();
				LOG.debug("Solution : " + soln);
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		} finally {
			qexec.close();
		}
	}
}
