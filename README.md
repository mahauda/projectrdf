## Projet RDF

### Intro

Projet qui encapsule la bibliothèque Jena afin de "faciliter" et "simplifier" des traitements en accès, lecture et écriture
sur les modèles RDF dans n'importe quelle langage (N3, RDF-XML, etc.).

### Ajout référence projet

Pour ajouter la référence de ce projet dans Maven, il faut ajouter les lignes suivantes dans le pom.xml :
```xml
<dependency>
	<groupId>universite.angers.master.info.rdf</groupId>
	<artifactId>project-rdf</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<scope>compile</scope>
</dependency>
```